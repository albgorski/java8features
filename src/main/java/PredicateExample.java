import java.util.List;
import java.util.function.Predicate;

import static java.util.Arrays.asList;

public class PredicateExample {
    public static Predicate<String> checkIfStartsWith(final String letter) {
        return name -> name.startsWith(letter);
    }

    public static Predicate<String> checkIfEndsWith(final String letter) {
        return name -> name.endsWith(letter);
    }

    public static void main(String[] args) {
        List<String> names = asList("Albert", "Joanna", "Oliwia", "Ohan");
        names.stream()
                .filter(
                        checkIfStartsWith("O").and(checkIfEndsWith("a"))
                )
                .forEach(
                        System.out::println
                );
    }
}
